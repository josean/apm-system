/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.common.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;

/**
 * @author JoseanLuo
 * @date 2017/10/11
 * @email joseanluo@gmail.com
 */
@Slf4j
public class EndPointInfo {

    private static int processNo;

    private static String ip;

    private static String hostName;

    private static String architecture;

    private static int processors;

    private static String systemName;

    private static String systemVersion;

    static {
        init();
    }

    private static void init() {
        try {
            OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
            architecture=operatingSystemMXBean.getArch();
            processors=operatingSystemMXBean.getAvailableProcessors();
            systemName=operatingSystemMXBean.getName();
            systemVersion=operatingSystemMXBean.getVersion();
            RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
            String name=runtimeMXBean.getName();
            processNo=Integer.parseInt(name.split("@")[0]);

        } catch (Exception e) {
          log.error("EndPointInfo get exception",e);
        }
    }

}
