/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.common.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author JoseanLuo
 * @date 2017/10/10
 * @email joseanluo@gmail.com
 */
public class StringUtil {

    public static boolean isEmpty(String str) {
        if (str == null || str.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * splicing the strings with delimiter
     *
     * @param delimiter String   delimiter
     * @param strings   strings
     */
    public static String joinWithDelimiter(final char delimiter, final String... strings) {
        if (null == strings || strings.length == 0) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(strings[0]);
        for (int i = 1; i < strings.length; i++) {
            stringBuilder.append(delimiter).append(strings[i]);
        }

        return stringBuilder.toString();
    }

    public static Map<String, String> argsParser(String args) {
        if (isEmpty(args)) {
            return Collections.emptyMap();
        }
        Map<String, String> map = new HashMap<String, String>();

        Scanner scanner = new Scanner(args);
        scanner.useDelimiter("\\s*,\\s*");
        while (scanner.hasNext()) {
            String token = scanner.next().trim();
            int assign = token.indexOf('=');

            if (assign == -1) {
                map.put(token, "");
            } else {
                String key = token.substring(0, assign);
                String value = token.substring(assign + 1);
                map.put(key, value);
            }
        }
        scanner.close();
        return Collections.unmodifiableMap(map);
    }


}
