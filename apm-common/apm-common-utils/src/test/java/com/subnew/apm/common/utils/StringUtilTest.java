/**
 * Copyright (c) 2016-2018  ghbank.com.cn Corporation Limited .
 * All Rights Reserved
 * This software is the confidential and proprietary information of Ghbank Company
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with ghbank.com.cn
 */
package com.subnew.apm.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * @author JoseanLuo
 * @date 2017/10/10
 * @email joseanluo@gmail.com
 */
public class StringUtilTest {
    @Test
    public void testIsEmpty() {
        Assert.assertTrue(StringUtil.isEmpty(null));
        Assert.assertTrue(StringUtil.isEmpty(""));
        Assert.assertTrue(StringUtil.isEmpty("   "));
        Assert.assertFalse(StringUtil.isEmpty(" A "));
    }

    @Test
    public void testIsNotEmpty() {
        Assert.assertFalse(StringUtil.isNotEmpty(null));
        Assert.assertFalse(StringUtil.isNotEmpty(""));
        Assert.assertFalse(StringUtil.isNotEmpty("   "));
        Assert.assertTrue(StringUtil.isNotEmpty(" A "));
    }

    @Test
    public void testJoinWithDelimiter() {
        Assert.assertNull(StringUtil.joinWithDelimiter('.'));
        Assert.assertEquals("one.", StringUtil.joinWithDelimiter('.', "one."));
        Assert.assertEquals("one.two", StringUtil.joinWithDelimiter('.', "one","two"));
    }
    @Test
    public void testArgsParser(){
        String agrs="";
        Map<String,String> map=StringUtil.argsParser(agrs);
        Assert.assertTrue(map.isEmpty());
        agrs="a=abc,b=b";
        map=StringUtil.argsParser(agrs);
        Assert.assertTrue("abc".equals(map.get("a")));
        Assert.assertTrue("b".equals(map.get("b")));
    }

}
