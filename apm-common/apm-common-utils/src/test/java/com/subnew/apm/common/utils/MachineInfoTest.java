/**
 * Copyright (c) 2016-2018  ghbank.com.cn Corporation Limited .
 * All Rights Reserved
 * This software is the confidential and proprietary information of Ghbank Company
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with ghbank.com.cn
 */
package com.subnew.apm.common.utils;

import org.junit.Test;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;

/**
 *  @author JoseanLuo
 *  @date 2017/10/10
 *  @email joseanluo@gmail.com
 */
public class MachineInfoTest {
    @Test
    public void testGetProcessNo(){
        System.out.println(MachineInfo.getProcessNo());
        System.out.println(MachineInfo.getHostName());
        System.out.println(MachineInfo.getHostIp());
        System.out.println(MachineInfo.getHostDesc());
        //
        OperatingSystemMXBean op = ManagementFactory.getOperatingSystemMXBean();
        System.out.println("Architecture: " + op.getArch());
        System.out.println("Processors: " + op.getAvailableProcessors());
        System.out.println("System name: " + op.getName());
        System.out.println("System version: " + op.getVersion());
        //
        RuntimeMXBean mxbean = ManagementFactory.getRuntimeMXBean();
        System.out.println("jvm name:" + mxbean.getName());
        System.out.println("jvm name:" + mxbean.getVmName());
        System.out.println("jvm version:" + mxbean.getVmVersion());
        System.out.println("jvm bootClassPath:" + mxbean.getBootClassPath());
        System.out.println("jvm start time:" + mxbean.getStartTime());
       // mxbean.get
    }
}
