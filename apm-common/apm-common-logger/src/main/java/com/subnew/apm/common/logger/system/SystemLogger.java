/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.common.logger.system;

import com.subnew.apm.common.logger.Level;
import com.subnew.apm.common.logger.Logger;
import com.subnew.apm.common.logger.LoggerFactory;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.MessageFormat;

/**
 * @author JoseanLuo
 * @date 2017/10/25
 * @email joseanluo@gmail.com
 */
public class SystemLogger implements Logger, Serializable {
    private static final long serialVersionUID = 1L;
    private final PrintStream out=System.out;
    private final PrintStream err=System.err;
    private String messagePattern = "{0,date,yyyy-MM-dd HH:mm:ss}"+SystemLogger.class.getName()+" [{1}]{2} {3}";
    private MessageFormat messageFormat;
    public  final static String LEVEL_FORMAT="[%s]";


    public SystemLogger(String  name){
        this.messagePattern="{0,date,yyyy-MM-dd HH:mm:ss} "+name+" [{1}]{2} {3}";
        messageFormat=new MessageFormat(messagePattern);
    }
    private String toString(Throwable throwable) {
        if (throwable == null) {
            return "";
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        pw.println();
        throwable.printStackTrace(pw);
        pw.close();
        return sw.toString();
    }

    public void trace(String msg) {
        Object[] parameter = {System.currentTimeMillis(), Level.TRACE, msg, ""};
        out.println(messageFormat.format(parameter));
    }

    public void trace(Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.TRACE, "", toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void trace(String msg, Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.TRACE, msg, toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void debug(String msg) {
        Object[] parameter = {System.currentTimeMillis(), Level.DEBUG, msg, ""};
        out.println(messageFormat.format(parameter));
    }

    public void debug(Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.DEBUG, "", toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void debug(String msg, Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.DEBUG, msg, toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void info(String msg) {
        Object[] parameter = {System.currentTimeMillis(), Level.INFO, msg, ""};
        out.println(messageFormat.format(parameter));
    }

    public void info(Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.INFO, "", toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void info(String msg, Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.INFO, msg, toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void warn(String msg) {
        Object[] parameter = {System.currentTimeMillis(), Level.WARN, msg, ""};
        out.println(messageFormat.format(parameter));
    }

    public void warn(Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.WARN, "", toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void warn(String msg, Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.WARN, msg, toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void error(String msg) {
        Object[] parameter = {System.currentTimeMillis(), Level.ERROR, msg, ""};
        out.println(messageFormat.format(parameter));
    }

    public void error(Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.ERROR, "", toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public void error(String msg, Throwable e) {
        Object[] parameter = {System.currentTimeMillis(), Level.ERROR, msg, toString(e)};
        out.println(messageFormat.format(parameter));
    }

    public boolean isTraceEnabled() {
        Level level=LoggerFactory.getLevel();
        if(level.equals(Level.TRACE)||level.equals(Level.ALL)){
            return true;
        }
        return false;
    }

    public boolean isDebugEnabled() {
        Level level=LoggerFactory.getLevel();
        if(level.equals(Level.TRACE)||level.equals(Level.ALL)||level.equals(Level.DEBUG)){
            return true;
        }
        return false;
    }

    public boolean isInfoEnabled() {
        Level level=LoggerFactory.getLevel();
        if(level.equals(Level.ERROR)||level.equals(Level.ERROR)||level.equals(Level.OFF)){
            return false;
        }
        return true;
    }

    public boolean isWarnEnabled() {
        Level level=LoggerFactory.getLevel();
        if(level.equals(Level.ERROR)||level.equals(Level.ERROR)){
            return true;
        }
        return false;
    }

    public boolean isErrorEnabled() {
        Level level=LoggerFactory.getLevel();
        if(level.equals(Level.ERROR)){
            return true;
        }
        return false;
    }
}
