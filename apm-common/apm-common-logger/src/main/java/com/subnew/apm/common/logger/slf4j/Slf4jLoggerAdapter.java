/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.common.logger.slf4j;

import com.subnew.apm.common.logger.Level;
import com.subnew.apm.common.logger.Logger;
import com.subnew.apm.common.logger.LoggerAdapter;
import lombok.Getter;
import lombok.Setter;

/**
 *  @author JoseanLuo
 *  @date 2017/10/25
 *  @email joseanluo@gmail.com
 */
public class Slf4jLoggerAdapter implements LoggerAdapter {
    @Getter
    @Setter
    private Level level;

    public Logger getLogger(Class<?> key) {
        return  new Slf4jLogger(org.slf4j.LoggerFactory.getLogger(key));
    }

    public Logger getLogger(String key) {
        return new Slf4jLogger(org.slf4j.LoggerFactory.getLogger(key));
    }


}
