/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.test;

import com.subnew.apm.sniffer.core.boot.BootServiceManager;
import org.junit.rules.ExternalResource;

/**
 *  @author JoseanLuo
 *  @date 2017/10/11
 *  @email joseanluo@gmail.com
 */
public class BootServiceRule extends ExternalResource {
    @Override
    protected void after() {
        super.after();
    }

    @Override
    protected void before() throws Throwable {
        super.before();
        BootServiceManager.getInstance().boot();
    }
}
