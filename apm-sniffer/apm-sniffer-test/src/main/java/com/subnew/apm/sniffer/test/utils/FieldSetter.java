/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.test.utils;

import java.lang.reflect.Field;

/**
 * Created by xin on 2017/7/9.
 */
public class FieldSetter {

    public static <T> void setValue(Object instance,
        String fieldName, T value) throws IllegalAccessException, NoSuchFieldException {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(instance, value);
    }

    public static <T> void setStaticValue(Class instance,
        String fieldName, T value) throws IllegalAccessException, NoSuchFieldException {
        Field field = instance.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(instance, value);
    }


}
