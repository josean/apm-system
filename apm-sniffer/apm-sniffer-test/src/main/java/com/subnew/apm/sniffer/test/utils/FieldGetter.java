/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.test.utils;

import java.lang.reflect.Field;

public class FieldGetter {
    public static <T> T getValue(Object instance,
        String fieldName) throws IllegalAccessException, NoSuchFieldException {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T)field.get(instance);
    }

    public static <T> T getParentFieldValue(Object instance,
        String fieldName) throws IllegalAccessException, NoSuchFieldException {
        Field field = instance.getClass().getSuperclass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T)field.get(instance);
    }


    public static <T> T get2LevelParentFieldValue(Object instance,
        String fieldName) throws IllegalAccessException, NoSuchFieldException {
        Field field = instance.getClass().getSuperclass().getSuperclass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T)field.get(instance);
    }
}
