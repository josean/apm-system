/**
 * Copyright (c) 2016-2018  ghbank.com.cn Corporation Limited .
 * All Rights Reserved
 * This software is the confidential and proprietary information of Ghbank Company
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with ghbank.com.cn
 */
package com.subnew.apm.sniffer.dubbo;

import com.subnew.apm.sniffer.core.plugin.AbstractPlugin;
import com.subnew.apm.sniffer.core.plugin.Plugin;

/**
 *  @author JoseanLuo
 *  @date 2017/11/2
 *  @email joseanluo@gmail.com
 */
public class DubboPlugin  extends AbstractPlugin {


}
