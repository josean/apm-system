/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core.plugin;

/**
 * @author JoseanLuo
 * @date 2017/10/20
 * @email joseanluo@gmail.com
 */

public class PluginDefine {

    private String name;

    private String defineClass;

    private State state;

    private enum State {
        OFF, ON
    }
}