/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core.boot;

import lombok.Getter;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  @author JoseanLuo
 *  @date 2017/10/20
 *  @email joseanluo@gmail.com
 */

public class SnifferNamedThreadFactory  implements ThreadFactory {

    private final AtomicInteger threadSeq = new AtomicInteger(0);

    @Getter
    private  final String  namePrefix;

    public SnifferNamedThreadFactory(String name){
     namePrefix="sniffer-"+name;
    }

    public Thread newThread(Runnable r) {
        Thread thread=new Thread(r,namePrefix+threadSeq.incrementAndGet());
        thread.setDaemon(true);
        return thread;
    }
}