package com.subnew.apm.sniffer.core.interceptor;

/**
 * @author JoseanLuo
 * @date 2017/11/2
 * @email joseanluo@gmail.com
 */
public interface AroundInterceptor extends Interceptor {

    void before(Object target, Object[] arg);

    void after(Object target, Object[] arg, Object result, Throwable throwable);
}
