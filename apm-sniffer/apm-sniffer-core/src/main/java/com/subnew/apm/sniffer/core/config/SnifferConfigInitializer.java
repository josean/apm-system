/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core.config;

import com.subnew.apm.common.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author JoseanLuo
 * @date 2017/10/12
 * @email joseanluo@gmail.com
 */
@Slf4j
public class SnifferConfigInitializer {
    private static String CONFIG_FILE_NAME = "apm.config";

    public static void initialize() {
        InputStream configFileStream;

        configFileStream = loadConfigBySystemProperty();


    }

    private static InputStream loadConfigBySystemProperty() {
        String config = System.getProperty("config");
        if (StringUtil.isEmpty(config)) {
            return null;
        }
        File configFile = new File(config);
        if (configFile.exists() && configFile.isDirectory()) {
            configFile = new File(config, CONFIG_FILE_NAME);
        }
        if (configFile.exists() && configFile.isFile()) {
            log.info("Found ConfigFile[{}] ,according system property.", configFile.getAbsolutePath());
            try {
                return new FileInputStream(configFile);
            } catch (FileNotFoundException e) {
                log.error("FAIL to load ConfigFile[{}] ,according system property.", configFile.getAbsolutePath());
            }
        }
        log.info("No config file active [{}], according system property.", config);
        return null;
    }

    private static InputStream loadConfigFromSnifferFolder() {
        String agentBasePath = initSnifferBasePath();
        if (StringUtil.isEmpty(agentBasePath)) {
            return null;
        }
        File configFile = new File(agentBasePath, CONFIG_FILE_NAME);
        if (configFile.exists() && configFile.isFile()) {
            try {
                log.info("Found ConfigFile[{}]  in sniffer folder.", configFile.getAbsolutePath());
                return new FileInputStream(configFile);
            } catch (FileNotFoundException e) {
                log.error("FAIL to load ConfigFile[{}] in sniffer folder.", configFile.getAbsolutePath());
            }
        }
        log.info("No config file active in sniffer folder.");
        return null;
    }


    private static String initSnifferBasePath() {
        String classResourcePath = SnifferConfigInitializer.class.getName().replaceAll("\\.", "/") + ".class";
        URL resource = SnifferConfigInitializer.class.getClassLoader().getSystemClassLoader().getResource(classResourcePath);
        if (null != resource) {
            String urlString = resource.toString();

            File agentJarFile = null;
            try {
                urlString = urlString.substring(urlString.indexOf("file:"), urlString.indexOf('!'));
                log.info("Try to load config from [{}]", urlString);
                agentJarFile = new File(new URL(urlString).getFile());
                if (agentJarFile.exists()) {
                    return agentJarFile.getParentFile().getAbsolutePath();
                }
            } catch (Exception e) {
                log.error("Can't  locate agent jar file by url[{}]", urlString);
            }
        }
        log.info("No config file active from locate agent jar file ");
        return null;
    }


}
