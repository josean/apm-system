/**
 * Copyright (c) 2016-2018  ghbank.com.cn Corporation Limited .
 * All Rights Reserved
 * This software is the confidential and proprietary information of Ghbank Company
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with ghbank.com.cn
 */
package com.subnew.apm.sniffer.core.plugin;

import com.subnew.apm.sniffer.core.instrument.filter.ClassFileFilter;
import com.subnew.apm.sniffer.core.interceptor.Interceptor;
import lombok.Getter;
import lombok.Setter;

import java.security.ProtectionDomain;

/**
 * @author JoseanLuo
 * @date 2017/11/2
 * @email joseanluo@gmail.com
 */
public abstract class AbstractPlugin implements Plugin {
    @Getter
    @Setter
    private ClassFileFilter classFileFilter;
    @Getter
    @Setter
    private Interceptor interceptor;

    public void setup(ClassFileFilter classFileFilter, Interceptor interceptor) {
        this.classFileFilter = classFileFilter;
        this.interceptor = interceptor;
    }

    public boolean accept(ClassLoader classLoader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classFileBuffer) {
        return getClassFileFilter().accept(classLoader,className,classBeingRedefined,protectionDomain,classFileBuffer);
    }
}
