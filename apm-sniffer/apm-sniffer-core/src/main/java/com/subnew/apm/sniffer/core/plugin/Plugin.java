package com.subnew.apm.sniffer.core.plugin;

import com.subnew.apm.sniffer.core.instrument.filter.ClassFileFilter;
import com.subnew.apm.sniffer.core.interceptor.Interceptor;

import java.security.ProtectionDomain;

/**
 * 插件接口
 *
 * @author JoseanLuo
 * @date 2017/11/2
 * @email joseanluo@gmail.com
 */
public interface Plugin {

    void setup(ClassFileFilter classFileFilter, Interceptor interceptor);

    boolean accept(ClassLoader classLoader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classFileBuffer);
}

