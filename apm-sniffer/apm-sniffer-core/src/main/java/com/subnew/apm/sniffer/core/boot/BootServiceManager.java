/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core.boot;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author JoseanLuo
 * @date 2017/10/11
 * @email joseanluo@gmail.com
 */
@Slf4j
public class BootServiceManager {

    private static Map<Class, BootService> bootedServices = new HashMap<Class, BootService>();
    private static BootServiceManager instance;

    private BootServiceManager() {

    }

    public static BootServiceManager getInstance() {
        if (null == instance) {
            synchronized (bootedServices) {
                if (null == instance) {
                    instance = new BootServiceManager();
                }
            }
        }
        return instance;
    }

    public void boot() {
        init();
        beforeBoot();
        startup();
        afterBoot();
    }

    private void init() {
        Iterator<BootService> bootServiceIterator = ServiceLoader.load(BootService.class).iterator();
        while (bootServiceIterator.hasNext()) {
            BootService bootService = bootServiceIterator.next();
            bootedServices.put(bootService.getClass(), bootService);
        }
    }


    private void beforeBoot() {
        for (BootService bootService : bootedServices.values()) {
            try {
                bootService.beforeBoot();
            } catch (Throwable e) {
                log.error("BootServiceManager try to beforeBoot [{}] FAIL.", bootService.getClass().getName(), e);
            }
        }
    }

    private void startup() {
        for (BootService bootService : bootedServices.values()) {
            try {
                bootService.boot();
            } catch (Throwable e) {
                log.error("BootServiceManager try to boot [{}] FAIL.", bootService.getClass().getName(), e);
            }
        }
    }

    private void afterBoot() {
        for (BootService bootService : bootedServices.values()) {
            try {
                bootService.afterBoot();
            } catch (Throwable e) {
                log.error("BootServiceManager try to afterBoot [{}] FAIL.", bootService.getClass().getName(), e);
            }
        }
    }

    public void shutdown() {
        for (BootService bootService : bootedServices.values()) {
            try {
                bootService.shutdown();
            } catch (Throwable e) {
                log.error("BootServiceManager try to shutdown [{}] FAIL.", bootService.getClass().getName(), e);
            }
        }
    }

    public <T extends BootService> T getBootService(Class<T> serviceClass) {
        return (T) bootedServices.get(serviceClass);
    }


}
