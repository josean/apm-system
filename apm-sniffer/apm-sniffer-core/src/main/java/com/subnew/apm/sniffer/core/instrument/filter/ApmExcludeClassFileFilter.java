/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core.instrument.filter;

import java.security.ProtectionDomain;

/**
 * 排除自身包
 *
 * @author JoseanLuo
 * @date 2017/10/27
 * @email joseanluo@gmail.com
 */
public class ApmExcludeClassFileFilter implements ClassFileFilter {
    public boolean accept(ClassLoader classLoader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classFileBuffer) {
        if (className == null) {
            return SKIP;
        }

        if (className.startsWith("com/subnew/apm/sniffer/")) {
            return SKIP;
        }
        return ACCEPT;
    }
}
