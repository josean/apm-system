/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core.constants;

/**
 *  @author JoseanLuo
 *  @date 2017/10/12
 *  @email joseanluo@gmail.com
 */
public class Global {

    public static String PATH_SEPARATOR = System.getProperty("file.separator", "/");

    public static String LINE_SEPARATOR = System.getProperty("line.separator", "\n");
}
