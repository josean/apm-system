/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.apm.sniffer.core;

import com.subnew.apm.common.logger.Logger;
import com.subnew.apm.common.logger.LoggerFactory;
import com.subnew.apm.sniffer.core.constants.SnifferStateEnum;

import java.lang.instrument.Instrumentation;

/**
 * @author JoseanLuo
 * @date 2017/10/20
 * @email joseanluo@gmail.com
 */
public class ApmSniffer {

    private static final Logger log = LoggerFactory.getLogger(ApmSniffer.class);

    private static final SnifferStateEnum STATE = SnifferStateEnum.OFF;

    public static void premain(String agentArgs, Instrumentation instrumentation) {
        if (null == agentArgs) {
            agentArgs = "";
        }
        log.info("ApmSniffer  agentArgs:" + agentArgs);
        if (STATE.equals(SnifferStateEnum.FAIL)) {
            log.info("ApmSniffer current state is [FAIL],try to restart...." );
        }
        if(STATE.equals(SnifferStateEnum.OFF)){
            log.info("ApmSniffer current state is [OFF], start...." );
        }

        if (STATE.equals(SnifferStateEnum.ON)) {
            log.info("ApmSniffer current state is [ON],please do not restart!!!");
        }
        if(STATE.equals(SnifferStateEnum.STARTING)){
            log.info("ApmSniffer current state is [STARTING],please do not restart!!!" );
        }
    }

    public static void main(String[] args) {
        premain(null, null);
    }
}