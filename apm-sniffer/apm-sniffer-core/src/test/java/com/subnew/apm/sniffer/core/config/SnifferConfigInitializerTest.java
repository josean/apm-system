/**
 * Copyright (c) 2016-2018  ghbank.com.cn Corporation Limited .
 * All Rights Reserved
 * This software is the confidential and proprietary information of Ghbank Company
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with ghbank.com.cn
 */
package com.subnew.apm.sniffer.core.config;

import org.junit.Test;

import java.net.URL;

/**
 *  @author JoseanLuo
 *  @date 2017/10/12
 *  @email joseanluo@gmail.com
 */
public class SnifferConfigInitializerTest {

    @Test
    public void testInitSnifferBasePath(){
        //com/subnew/apm/sniffer/core/config/SnifferConfigInitializer.class
        String classResourcePath=SnifferConfigInitializerTest.class.getName().replaceAll("\\.", "/") + ".class";
        //com.subnew.apm.sniffer.core.config.SnifferConfigInitializer
        //classResourcePath=SnifferConfigInitializer.class.getName();
        System.out.println(classResourcePath);
        URL resource = SnifferConfigInitializerTest.class.getClassLoader().getSystemClassLoader().getResource(classResourcePath);
        //file:/D:/Repository/GIT/gitos/apm-system/apm-sniffer/apm-sniffer-core/target/test-classes/com/subnew/apm/sniffer/core/config/SnifferConfigInitializerTest.class
        String urlString= resource.toString();
        System.out.println(urlString);
       // urlString = urlString.substring(urlString.indexOf("file:"), urlString.indexOf('!'));
       // System.out.println(urlString);
    }
}
