package com.subnew.apm.extension;

/**
 * @author JoseanLuo
 * @date 2017/11/2
 * @email joseanluo@gmail.com
 */
@SPI
public interface ExtensionFactory {

    /**
     * Get extension.
     *
     * @param type object type.
     * @param name object name.
     * @return object instance.
     */
    <T> T getExtension(Class<T> type, String name);
}
