package com.subnew.apm.extension;

import java.net.URL;

/**
 * @author JoseanLuo
 * @date 2017/10/10
 * @email joseanluo@gmail.com
 */
public interface NoSpiExt {
    @Adaptive
    String echo(URL url, String s);

}
