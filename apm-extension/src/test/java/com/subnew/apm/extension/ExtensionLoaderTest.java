/**
 * Copyright (c) 2016-2018  ghbank.com.cn Corporation Limited .
 * All Rights Reserved
 * This software is the confidential and proprietary information of Ghbank Company
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with ghbank.com.cn
 */
package com.subnew.apm.extension;

import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author JoseanLuo
 * @date 2017/11/2
 * @email joseanluo@gmail.com
 */
public class ExtensionLoaderTest {

    @Test
    public void testGetExtensionLoader() {
        //空扩展点测试
        try {
            ExtensionLoader.getExtensionLoader(null);
            fail();
        } catch (Exception e) {
            assertThat(e.getMessage(), containsString("Extension type == null"));
        }
        //不为接口
        try {
            ExtensionLoader.getExtensionLoader(ExtensionLoaderTest.class);
            fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected.getMessage(), containsString("is not interface"));
        }
        //没有SPI注解
        try {
            ExtensionLoader.getExtensionLoader(NoSpiExt.class);
            fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected.getMessage(), containsString("WITHOUT @SPI Annotation"));
        }

    }
}
